package com.example.multicanalservices.tourguide;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TourismFragment extends Fragment {


    public TourismFragment() {
        // Required empty public constructor
    }


    @SuppressLint("StringFormatInvalid")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.zone_list, container, false);

        final ArrayList<Zone> zones = new ArrayList<>();
        zones.add(new Zone("La pagode", getResources().getString(R.string.lapagoge), R.drawable.lapagoge));
        zones.add(new Zone("Le monument de la Liberté", rootView.getResources().getString(R.string.monument_liberte), R.drawable.liberte));
        zones.add(new Zone("Musée maritime", rootView.getResources().getString(R.string.musee_maritime), R.drawable.musee));
        zones.add(new Zone("La Cathédrale Saint Pierre et Saint Paul", rootView.getResources().getString(R.string.cathedrale), R.drawable.cathedrale));


        ZoneAdapter adapter = new ZoneAdapter(getActivity(), zones, R.color.default_category);
        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Zone zone = zones.get(position);
                Intent zoneDetailIntent = new Intent(getActivity(), ZoneDetailActivity.class);
                zoneDetailIntent.putExtra("zoneDetail", zone.getZoneDetails());
                zoneDetailIntent.putExtra("zone", zone.getZone());
                if (zone.isIssetImage()){
                    zoneDetailIntent.putExtra("zoneImage", zone.getZoneImageResourceId());
                }
                startActivity(zoneDetailIntent);
            }
        });
        return rootView;
    }

}
