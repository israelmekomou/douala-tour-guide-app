package com.example.multicanalservices.tourguide;

public class Zone {

    private boolean issetImage;

    private String tZone;

    private String tZoneDetails;

    private int tZoneImageResourceId;

    public Zone(String zone, String zoneDetail, int zoneImageResourceId){
        tZone = zone;
        tZoneDetails = zoneDetail;
        tZoneImageResourceId = zoneImageResourceId;
        issetImage = true;
    }

    public Zone(String zone, String zoneDetail){
        tZone = zone;
        tZoneDetails = zoneDetail;
        issetImage = false;
    }

    public int getZoneImageResourceId() {
        return tZoneImageResourceId;
    }

    public String getZone() {
        return tZone;
    }

    public String getZoneDetails() {
        return tZoneDetails;
    }

    public boolean isIssetImage() {
        return issetImage;
    }
}
