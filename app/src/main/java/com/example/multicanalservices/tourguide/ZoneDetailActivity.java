package com.example.multicanalservices.tourguide;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("Registered")
public class ZoneDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_detail);

        Intent intent = getIntent();

        if (intent != null){
            String strZoneDetail = "";
            String strZone = "";
            if (intent.hasExtra("zone")){
                strZone = intent.getStringExtra("zone");
            }
            if (intent.hasExtra("zoneDetail")){
                strZoneDetail = intent.getStringExtra("zoneDetail");
            }
            if (intent.hasExtra("zoneImage")){
                int imgResource = intent.getIntExtra("zoneImage", 0);
                ImageView imageView = findViewById(R.id.card_image);
                imageView.setImageResource(imgResource);
            }

            TextView zoneTextView = findViewById(R.id.card_title);
            zoneTextView.setText(strZone);

            TextView zoneDetailTextView = findViewById(R.id.detail_text_view);
            zoneDetailTextView.setText(strZoneDetail);
        }
    }
}
