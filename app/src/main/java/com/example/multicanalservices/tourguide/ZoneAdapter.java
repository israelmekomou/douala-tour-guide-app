package com.example.multicanalservices.tourguide;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ZoneAdapter extends ArrayAdapter<Zone> {
    private int tColorResourceId;

    public ZoneAdapter(Activity context, ArrayList<Zone> zones, int colorResourceId){
        super(context, 0, zones);
        tColorResourceId = colorResourceId;
    }

    @androidx.annotation.NonNull
    @Override
    public View getView(int position, @androidx.annotation.Nullable View convertView, @androidx.annotation.NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        Zone currentZone = getItem(position);

        TextView zoneTextView = listItemView.findViewById(R.id.zone_text_view);

        assert currentZone != null;
        zoneTextView.setText(currentZone.getZone());

        View textContainer = listItemView.findViewById(R.id.layout);

        int color = ContextCompat.getColor(getContext(), tColorResourceId);

        textContainer.setBackgroundColor(color);

        ImageView imageView = listItemView.findViewById(R.id.image);
        imageView.setImageResource(currentZone.getZoneImageResourceId());

        if (currentZone.isIssetImage()){
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }

        return listItemView;
    }
}
