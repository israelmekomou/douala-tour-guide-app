package com.example.multicanalservices.tourguide;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantFragment extends Fragment {


    public RestaurantFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.zone_list, container, false);

        final ArrayList<Zone> zones = new ArrayList<>();
        zones.add(new Zone("L'Ovalie", rootView.getResources().getString(R.string.ovalie), R.drawable.ovalie));
        zones.add(new Zone("La Fourchette", rootView.getResources().getString(R.string.lafourchette), R.drawable.lafourchette));
        zones.add(new Zone("Le C'",rootView.getResources().getString(R.string.lec), R.drawable.lec));
        zones.add(new Zone("Le Tournebroche",rootView.getResources().getString(R.string.tournebroche), R.drawable.tournebroche));
        zones.add(new Zone("La Pizzeria",rootView.getResources().getString(R.string.pizerria), R.drawable.pizerria));
        zones.add(new Zone("5 Fourchettes",rootView.getResources().getString(R.string.fourchette), R.drawable.fourchette));
        zones.add(new Zone("Bombay Masala",rootView.getResources().getString(R.string.bombay), R.drawable.bombay));
        zones.add(new Zone("Saga Africa",rootView.getResources().getString(R.string.saga), R.drawable.saga));
        zones.add(new Zone("Le Mediterranee",rootView.getResources().getString(R.string.mediteran), R.drawable.mediteran));
        zones.add(new Zone("Moulin de France",rootView.getResources().getString(R.string.moulin), R.drawable.moulin));

        ZoneAdapter adapter = new ZoneAdapter(getActivity(), zones, R.color.default_category);
        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Zone zone = zones.get(position);
                Intent zoneDetailIntent = new Intent(getActivity(), ZoneDetailActivity.class);
                zoneDetailIntent.putExtra("zoneDetail", zone.getZoneDetails());
                zoneDetailIntent.putExtra("zone", zone.getZone());
                if (zone.isIssetImage()){
                    zoneDetailIntent.putExtra("zoneImage", zone.getZoneImageResourceId());
                }
                startActivity(zoneDetailIntent);
            }
        });
        return rootView;
    }

}
